# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.model import ModelView, fields
from trytond.modules.account_col.tax import Tax
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

_ZERO = Decimal(0)


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
    ])
    party_region_local_contract = fields.Selection([
        ('', ''),
        ('yes', 'Yes'),
        ('not', 'Not')],
        'Party in Region of Contract', states={
        'invisible': Eval('type') != 'in',
    })
    good_service_supplied = fields.Many2One(
        'account.invoice.good_service_supplied',
        'Good or Service Supplied', states={
            'invisible': Eval('type') != 'in',
        })
    num_invoice_supplier = fields.Char('No. Invoice Supplier',
            states={'invisible': Eval('type') != 'in'})

    # @fields.depends('analytic_account', 'project')
    # def on_change_project(self):
    #     if self.project and self.project.analytic_account:
    #         self.analytic_account = self.project.analytic_account


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('product', '_parent_invoice.project', 'analytic_accounts', 'analytic_account')
    def on_change_product(self):
        super().on_change_product()
        if self.product and self.invoice.project and self.invoice.project.analytic_account:
            self.analytic_account = self.invoice.project.analytic_account
            for analytic in self.analytic_accounts:
                analytic.account = self.analytic_account
        else:
            self.analytic_account = None
            for analytic in self.analytic_accounts:
                analytic.account = None


class InvoiceAddProjectStart(ModelView):
    "Invoice Add Project Start"
    __name__ = 'account_invoice.add_project.start'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
    ])


class InvoiceAddProject(Wizard):
    "Invoice Add Project"
    __name__ = 'account_invoice.add_project'
    start = StateView(
        'account_invoice.add_project.start',
        'invoice_project.add_project_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        project_id = self.start.project.id

        for id_ in ids:
            query = "UPDATE account_invoice SET project=%s WHERE id=%s"
            cursor.execute(query % (project_id, id_))
        return 'end'


class InvoiceTaxesByProjectStart(ModelView):
    "Invoice Taxes By Project Start"
    __name__ = 'invoice_project.invoice_taxes_by_project.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        depends=['fiscalyear'], required=True, domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ])
    end_period = fields.Many2One('account.period', 'End Period',
        depends=['fiscalyear'], required=True, domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ])
    detailed = fields.Boolean('Detailed')
    classification = fields.Selection(Tax.classification.selection,
        'Classification')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), test_state=False).id

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class InvoiceTaxesByProject(Wizard):
    "Invoice Taxes By Project Start"
    __name__ = 'invoice_project.invoice_taxes_by_project'
    start = StateView(
        'invoice_project.invoice_taxes_by_project.start',
        'invoice_project.print_invoice_taxes_by_project_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'open_', 'tryton-ok', default=True),
        ])
    open_ = StateReport('invoice_project.invoice_taxes_by_project_report')

    def do_open_(self, action):
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'start_period': self.start.start_period.id,
            'end_period': self.start.end_period.id,
            'detailed': self.start.detailed,
            'classification': self.start.classification,
        }
        return action, data

    def transition_open_(self):
        return 'end'


class InvoiceTaxesByProjectReport(Report):
    "Invoice By Party Report"
    __name__ = 'invoice_project.invoice_taxes_by_project_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        Period = pool.get('account.period')
        Fiscalyear = pool.get('account.fiscalyear')
        company = Company(data['company'])

        start_period = Period(data['start_period'])
        end_period = Period(data['end_period'])
        periods = Period.search([
            ('fiscalyear', '=', data['fiscalyear']),
            ('start_date', '>=', start_period.start_date),
            ('start_date', '<=', end_period.start_date),
        ])
        periods_ids = [p.id for p in periods]
        invoices = Invoice.search([
            ('company', '=', data['company']),
            ('move.period.fiscalyear', '=', data['fiscalyear']),
            ('move.period', 'in', periods_ids),
            ('state', 'in', ['posted', 'paid']),
        ])
        projects = {
            '0': {
                'name': 'NP',
                'taxes': {},
            },
        }

        def _add_taxes(invoice_number, tax_dict, invoice_taxes, party):
            for inv_tax in invoice_taxes:
                if inv_tax.tax.id not in tax_dict.keys():
                    if data['classification'] and data['classification'] != inv_tax.tax.classification:
                        continue
                    tax_dict[inv_tax.tax.id] = {
                        'name': inv_tax.tax.name,
                        'base': [],
                        'amount': [],
                        'parties': {},
                    }
                tax_dict[inv_tax.tax.id]['base'].append(inv_tax.base)
                tax_dict[inv_tax.tax.id]['amount'].append(inv_tax.amount)
                if data['detailed']:
                    if party.id not in tax_dict[inv_tax.tax.id]['parties'].keys():
                        tax_dict[inv_tax.tax.id]['parties'][party.id] = {
                            'name': party.name,
                            'base': [],
                            'amount': [],
                            'invoice': '',
                        }
                    tax_dict[inv_tax.tax.id]['parties'][party.id]['base'].append(inv_tax.base)
                    tax_dict[inv_tax.tax.id]['parties'][party.id]['amount'].append(inv_tax.amount)
                    tax_dict[inv_tax.tax.id]['parties'][party.id]['invoice'] = invoice_number

        for invoice in invoices:
            if not invoice.project:
                _add_taxes(invoice.number, projects['0']['taxes'], invoice.taxes, invoice.party)
                continue
            if invoice.project.id not in projects:
                projects[invoice.project.id] = {
                    'name': invoice.project.name,
                    'taxes': {},
                }
            _add_taxes(invoice.number, projects[invoice.project.id]['taxes'], invoice.taxes, invoice.party)

        records = []
        for project in projects.values():
            if project['taxes']:
                records.append(project)
        report_context['records'] = records
        report_context['start_period'] = start_period.name
        report_context['end_period'] = end_period.name
        report_context['fiscalyear'] = Fiscalyear(data['fiscalyear'])
        report_context['company'] = company.party.name
        return report_context


class AddAnalyticAccountStart(metaclass=PoolMeta):
    "Add Analytic Start"
    __name__ = 'account_invoice.add_analytic.start'
    targets = fields.Many2Many('analytic_account.account', None, None,
        'Targets')

    @classmethod
    def __setup__(cls):
        super(AddAnalyticAccountStart, cls).__setup__()
        cls.analytic_account.domain.extend([('id', 'in', Eval('targets'))])

    @staticmethod
    def default_targets():
        id_ = Transaction().context['active_id']
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoice = Invoice(id_)
        accounts = []
        if invoice.project and invoice.project.analytic_account:
            childs = list(invoice.project.analytic_account.childs)
            while childs:
                child = childs.pop()
                if child.type == 'normal':
                    accounts.append(child.id)
                else:
                    if child.childs:
                        childs.extend(child.childs)
        return accounts


class InvoicesProjectPurchaseStart(ModelView):
    "Invoices Purchase Project Start"
    __name__ = 'invoice_project.print_invoices_purchase.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    period_start = fields.Many2One('account.period', 'Period Start')
    period_end = fields.Many2One('account.period', 'Period End', required=True)
    type_inv = fields.Selection([
        ('in', 'Supplier Invoice'),
        ('in_credit_note', 'In Credit Note'),
    ], 'Type', required=True)
    project = fields.Many2One('project.work', 'Project')
    invoices_with_balance = fields.Boolean('Invoices With Balance to Pay',
        help='if check this option The report only print invoices with balance to pay')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('period_start', 'period_end')
    def on_change_period_start(self):
        self.period_end = self.period_start


class InvoicesProjectPurchase(Wizard):
    "Invoices Project Purchase"
    __name__ = 'invoice_project.print_invoices_purchase'
    start = StateView(
        'invoice_project.print_invoices_purchase.start',
        'invoice_project.print_invoices_purchase_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('invoice_project.invoices_purchase_report')

    def do_print_(self, action):
        period_start = None
        project = None
        if self.start.period_start:
            period_start = self.start.period_start.id

        if self.start.project:
            project = self.start.project.id
        data = {
            'company': self.start.company.id,
            'period_start': period_start,
            'period_end': self.start.period_end.id,
            'type_invoice': self.start.type_inv,
            'project': project,
            'invoices_with_balance': self.start.invoices_with_balance,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class InvoicesProjectPurchaseReport(Report):
    "Invoices Project Purchase Report"
    __name__ = 'invoice_project.invoices_purchase_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        Project = pool.get('project.work')
        PaymentTermDelta = pool.get('account.invoice.payment_term.line.delta')
        Period = pool.get('account.period')
        company = Company(data['company'])
        project = None
        period_start = None
        invoices_dom = []
        if data['period_start']:
            period_start = Period(data['period_start'])
        period_end = Period(data['period_end'])

        if data['type_invoice'] == "in_credit_note":
            type_invoice = 'in'
        else:
            type_invoice = data['type_invoice']

        invoices_dom = [
            ('company', '=', data['company']),
            ('move.period', '<=', period_end.id),
            ('type', '=', type_invoice),
            ('state', 'in', ['validated', 'posted', 'paid']),
        ]

        if data['project']:
            project = Project(data['project']).name
            invoices_dom.append(
                ('project.id', '=', data['project']),
            )
        if period_start:
            invoices_dom.append(
                ('move.period', '>=', period_start.id),
            )

        invoices = Invoice.search(invoices_dom)

        sum_untaxed_amount = []
        sum_invoice_tax_amount = []
        sum_total_amount = []
        invoices_filtered = []
        now = date.today()

        for invoice in invoices:
            if (invoice.untaxed_amount < _ZERO and "credit" not in data['type_invoice']) \
                or (invoice.untaxed_amount > _ZERO and "credit" in data['type_invoice']):
                continue
            if data['invoices_with_balance']:
                if invoice.amount_to_pay == 0:
                    continue
            paymentTermDelta = PaymentTermDelta.search([
                ('line.payment', '=', invoice.payment_term),
            ])
            days_expired = '0'
            balance_to_pay = 0
            balance_not_expired = 0
            if invoice.invoice_date and paymentTermDelta:
                if paymentTermDelta[0].days > 0:
                    days_expired = (now - invoice.invoice_date).days
                    days_expired = days_expired - paymentTermDelta[0].days

            if int(days_expired) <= 0:
                days_expired = '0'
                balance_not_expired = invoice.amount_to_pay
            else:
                balance_to_pay = invoice.amount_to_pay

            party_local = ''
            if invoice.party_region_local_contract == 'yes':
                party_local = 'Si'
            elif invoice.party_region_local_contract == 'not':
                party_local = 'No'
            else:
                party_local = ''

            dv = cls._get_check_digit(invoice.party.id_number, invoice.party.type_document)
            commercial_name = invoice.party.commercial_name or ' '
            if invoice.party.commercial_name and invoice.party.type_document == '13':
                commercial_name = '/ ' + invoice.party.commercial_name

            m = 0

            if invoice.invoice_date:
                m = int(invoice.invoice_date.month)
            months = [
                ' ', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO',
                'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE',
                'NOVIEMBRE', 'DICIEMBRE']
            month = months[m]

            value = {
                'invoice': invoice,
                'commercial_name': commercial_name,
                'days_expired': days_expired,
                'party_local': party_local,
                'balance_not_expired': balance_not_expired,
                'balance_to_pay': balance_to_pay,
                'check_digit': dv,
                'month': month,
            }
            invoices_filtered.append(value)

            sum_untaxed_amount.append(invoice.untaxed_amount)
            sum_invoice_tax_amount.append(invoice.tax_amount)
            sum_total_amount.append(invoice.total_amount)

        report_context['records'] = invoices_filtered
        report_context['sum_untaxed_amount'] = sum(sum_untaxed_amount)
        report_context['sum_total_amount'] = sum(sum_total_amount)
        report_context['sum_invoice_tax_amount'] = sum(sum_invoice_tax_amount)
        report_context['invoices_filtered'] = invoices_filtered
        report_context['project'] = project
        report_context['company'] = company.party.name

        return report_context

    @classmethod
    def _get_check_digit(cls, id_number, type_document):
        PRIMOS = [71, 67, 59, 53, 47, 43, 41, 37, 29, 23, 19, 17, 13, 7, 3]
        id_number = id_number.replace(".", "")
        if not id_number.isdigit() or type_document == '13':
            return ' '
        c = 0
        p = len(PRIMOS) - 1
        for n in reversed(id_number):
            c += int(n) * PRIMOS[p]
            p -= 1

        dv = c % 11
        if dv > 1:
            dv = 11 - dv
        return '-' + str(dv)


class PortfolioDetailedStart(metaclass=PoolMeta):
    "Portfolio Detailed Start"
    __name__ = 'invoice_report.portfolio_detailed.start'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
    ])


class PortfolioDetailed(metaclass=PoolMeta):
    "Portfolio Detailed"
    __name__ = 'invoice_report.portfolio_detailed'

    def do_print_(self, action):
        action, data = super(PortfolioDetailed, self).do_print_(action)
        if self.start.project:
            data['project'] = self.start.project.id,
        return action, data


class PortfolioDetailedReport(metaclass=PoolMeta):
    __name__ = 'party.portfolio_detailed.report'

    @classmethod
    def get_domain_invoice(cls, domain, header, data):
        domain = super().get_domain_invoice(domain, header, data)
        if data.get('project'):
            domain.append(
                ('project', '=', data['project']),
            )
        return domain
