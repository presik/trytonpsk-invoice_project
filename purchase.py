# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
    ])

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls._buttons.update({
            'add_analytic': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @classmethod
    @ModelView.button_action('invoice_project.wizard_purchase_add_analytic')
    def add_analytic(cls, records):
        pass

    def create_invoice(self):
        invoice = super(Purchase, self).create_invoice()
        if invoice and self.project:
            invoice.project = self.project.id
            invoice.save()
        return invoice

    # @fields.depends('analytic_account', 'project')
    # def on_change_project(self):
    #     if self.project and self.project.analytic_account:
    #         self.analytic_account = self.project.analytic_account


class AddPurchaseAnalyticAccountStart(ModelView):
    "Add Purchase Analytic Account Start"
    __name__ = 'purchase.add_analytic.start'
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', required=True, domain=[
            ('type', '=', 'normal'),
            ('id', 'in', Eval('targets')),
        ])
    targets = fields.Many2Many('analytic_account.account', None, None,
        'Targets')

    @staticmethod
    def default_targets():
        id_ = Transaction().context['active_id']
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        purchase = Purchase(id_)
        accounts = []
        if purchase.project and purchase.project.analytic_account:
            childs = list(purchase.project.analytic_account.childs)
            while childs:
                child = childs.pop()
                if child.type == 'normal':
                    accounts.append(child.id)
                else:
                    if child.childs:
                        childs.extend(child.childs)
        return accounts


class AddPurchaseAnalyticAccount(Wizard):
    "Add Purchase Analytic Account"
    __name__ = 'purchase.add_analytic'
    start = StateView(
        'purchase.add_analytic.start',
        'invoice_project.view_wizard_purchase_add_analytic_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        id_ = Transaction().context['active_id']
        Purchase = Pool().get('purchase.purchase')
        purchase = Purchase(id_)
        account_id = self.start.analytic_account.id

        for line in purchase.lines:
            if line.type != 'line':
                continue
            for la in line.analytic_accounts:
                print('account_id ...', account_id)
                la.account = account_id
                la.save()
        return 'end'


class PurchaseRequisition(metaclass=PoolMeta):
    __name__ = 'purchase.requisition'
    request_date = fields.Date('Date Requisition')
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
    ])


class PurchaseRequisitionLine(metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'

    def compute_request(self):
        request = super(PurchaseRequisitionLine, self).compute_request()
        employee = self.requisition.employee
        project = self.requisition.project
        number = self.requisition.number
        if project:
            project = project
        request.project = project
        request.employee = employee
        request.number = number
        return request


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    project = fields.Many2One('project.work', 'Project', readonly=True)
    employee = fields.Many2One('company.employee', 'Employee', readonly=True)


class CreatePurchase(metaclass=PoolMeta):
    """docstring for CreatePurchase."""
    @classmethod
    def _group_purchase_key(requests, request):
        super(CreatePurchase)._group_purchase_key(requests, request)
        return 'end'
